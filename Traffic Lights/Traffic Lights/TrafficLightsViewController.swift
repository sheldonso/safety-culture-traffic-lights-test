//
//  TrafficLightsViewController.swift
//  Traffic Lights
//
//  Created by Sheldon So on 7/05/17.
//
//

import Foundation
import UIKit

class TrafficLightsViewController: UIViewController {
    
    // MARK:- Storyboard UI
    @IBOutlet weak var northTrafficLightImageView: UIImageView!
    @IBOutlet weak var westTrafficLightImageView: UIImageView!
    @IBOutlet weak var southTrafficLightImageView: UIImageView!
    @IBOutlet weak var eastTrafficLightImageView: UIImageView!
    
    @IBOutlet weak var startStopButton: UIButton!
    
    
    // MARK:- Variables
    var northState: TrafficLightState = .notSet {
        didSet {
            self.updateImage(imageView: self.northTrafficLightImageView, newState: northState)
        }
    }
    var westState: TrafficLightState = .notSet {
        didSet {
            self.updateImage(imageView: self.westTrafficLightImageView, newState: westState)
        }
    }
    var southState: TrafficLightState = .notSet {
        didSet {
            self.updateImage(imageView: self.southTrafficLightImageView, newState: southState)
        }
    }
    var eastState: TrafficLightState = .notSet {
        didSet {
            self.updateImage(imageView: self.eastTrafficLightImageView, newState: eastState)
        }
    }
    
    var buttonState: StartStopState = .notSet {
        didSet {
            self.updateButton(newState: buttonState)
        }
    }
    
    var allowingCardinalDirectionsState: CardinalDirectionState = .none {
        didSet {
            self.updateTrafficLights(newState: allowingCardinalDirectionsState)
        }
    }
    
    private var countdownSecondsDuration: TimeInterval!
    private var countdownSecondsTimer: Timer?
    private var animateLightsTimer: Timer?      // Timer that updates the traffic lights images
    let trafficLightSwitchDuration = TimeInterval(30)
    let yellowLightDuration = TimeInterval(5)
    
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setupUI()
    }
    
    private func setupUI() {
        self.northTrafficLightImageView.image = UIImage(named: "red")
        self.westTrafficLightImageView.image = UIImage(named: "red")
        self.southTrafficLightImageView.image = UIImage(named: "red")
        self.eastTrafficLightImageView.image = UIImage(named: "red")
        
        self.startStopButton.setTitle("START", for: .normal)
    }
    
    
    // MARK:- Timer Methods
    @objc private func updateTrafficLightsState() {
        switch (self.allowingCardinalDirectionsState) {
        case .none:
            // Status is not set, switch on the traffic lights on east and west
            self.allowingCardinalDirectionsState = .eastWest
        case .eastWest:
            // State is now east and west, update the new state to south and north
            self.allowingCardinalDirectionsState = .southNorth
        case .southNorth:
            // State is now south and north, update the new state to east and west
            self.allowingCardinalDirectionsState = .eastWest
        }
    }
    
    @objc private func countdownAndUpdateButtonTitle() {
        // No need to update button title when counter is 0
        if (self.countdownSecondsDuration <= 0) {
            return
        }
        
        // Update UI on main thread
        DispatchQueue.main.async {
            if (self.buttonState == .starting) {
                // Update button title when the animation is starting
                self.startStopButton.setTitle(String(format: "STARTING IN %d", Int(self.countdownSecondsDuration!)), for: .normal)
            } else if (self.buttonState == .stopping) {
                // Update button title when the animation is stopping
                self.startStopButton.setTitle(String(format: "STOPPING IN %d", Int(self.countdownSecondsDuration!)), for: .normal)
            }
            self.countdownSecondsDuration = self.countdownSecondsDuration-1
        }
    }
    
    
    // MARK:- IBAction
    @IBAction func startStopButtonTapped(_ sender: UIButton) {
        switch self.buttonState {
        case .notSet, .stopped:                // Button tapped when state is not set or stopped
            // Schedule timer
            self.animateLightsTimer = Timer.scheduledTimer(timeInterval: trafficLightSwitchDuration+yellowLightDuration, target: self, selector: #selector(updateTrafficLightsState), userInfo: nil, repeats: true)
            
            // Update button
            self.buttonState = .starting
            
            // After a delay, update the traffic lights and button
            let waitDuration = DispatchTime.now() + self.yellowLightDuration
            DispatchQueue.main.asyncAfter(deadline: waitDuration, execute: {
                // Update button
                self.buttonState = .started
                // Update traffic lights
                self.allowingCardinalDirectionsState = .eastWest
                // Update countdown timer
                self.countdownSecondsTimer?.invalidate()
                self.countdownSecondsTimer = nil
            })
        case .starting, .stopping:
            // Do nothing, this shouldn't be triggered anyway
            break
        case .started:                          // Button tapped when traffic lights animation has started
            // Invalidate timer
            DispatchQueue.main.async {
                self.animateLightsTimer?.invalidate()
                self.animateLightsTimer = nil
            }
            
            // Update button
            self.buttonState = .stopping
            
            // Update traffic lights
            self.allowingCardinalDirectionsState = .none
            
            // After a delay, update the button
            let waitDuration = DispatchTime.now() + self.yellowLightDuration
            DispatchQueue.main.asyncAfter(deadline: waitDuration, execute: {
                // Update button
                self.buttonState = .stopped
                // Update countdown timer
                self.countdownSecondsTimer?.invalidate()
                self.countdownSecondsTimer = nil
            })
        }
    }
    
    
    // MARK:- Custom UI Methods
    private func updateImage(imageView: UIImageView, newState: TrafficLightState) {
        switch (newState) {
        case .notSet:
            // Do nothing
            break
        case .red:
            // Update UI in the main thread
            DispatchQueue.main.async {
                imageView.image = UIImage(named: "red")
            }
        case .amber:
            // Update UI in the main thread
            DispatchQueue.main.async {
                imageView.image = UIImage(named: "amber")
            }
        case .green:
            // Update UI in the main thread
            DispatchQueue.main.async {
                imageView.image = UIImage(named: "green")
            }
        }
    }
    
    private func updateButton(newState: StartStopState) {
        switch (newState) {
        case .notSet:
            // Do nothing
            break
        case .starting:
            self.countdownSecondsTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countdownAndUpdateButtonTitle), userInfo: nil, repeats: true)
            DispatchQueue.main.async {
                self.startStopButton.isEnabled = false
                self.countdownSecondsDuration = self.yellowLightDuration    // Reset counter
                self.countdownAndUpdateButtonTitle()                        // Execute method once before scheduled timer
            }
        case .started:
            DispatchQueue.main.async {
                self.startStopButton.isEnabled = true
                self.startStopButton.setTitle("STOP", for: .normal)
            }
        case .stopping:
            self.countdownSecondsTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countdownAndUpdateButtonTitle), userInfo: nil, repeats: true)
            DispatchQueue.main.async {
                self.startStopButton.isEnabled = false
                self.countdownSecondsDuration = self.yellowLightDuration    // Reset counter
                self.countdownAndUpdateButtonTitle()                        // Execute method once before scheduled timer
            }
        case .stopped:
            DispatchQueue.main.async {
                self.startStopButton.isEnabled = true
                self.startStopButton.setTitle("START", for: .normal)
            }
        }
    }
    
    private func updateTrafficLights(newState: CardinalDirectionState) {
        let waitDuration = DispatchTime.now() + self.yellowLightDuration
        
        switch (newState) {
        case .none:         // New traffic lights state is none, attempt to switch all traffic lights to red
            // If traffic light is either green or yellow, switch to yellow
            // If traffic light is already red, remain to be red
            self.eastState = self.eastState == .green || self.eastState == .amber ? .amber : .red
            self.southState = self.southState == .green || self.southState == .amber ? .amber : .red
            self.westState = self.westState == .green || self.westState == .amber ? .amber : .red
            self.northState = self.northState == .green || self.northState == .amber ? .amber : .red
            
            // After a delay, switch all traffic lights to red
            DispatchQueue.main.asyncAfter(deadline: waitDuration, execute: {
                self.eastState = .red
                self.southState = .red
                self.westState = .red
                self.northState = .red
            })
        case .eastWest:
            if ((self.southState == .green) && (self.northState == .green)) {
                // State switched from southNorth to eastWest
                self.southState = .amber
                self.northState = .amber
                
                DispatchQueue.main.asyncAfter(deadline: waitDuration, execute: {
                    if let _ = self.animateLightsTimer {                        
                        self.eastState = .green
                        self.southState = .red
                        self.westState = .green
                        self.northState = .red
                    }
                })
            } else {
                // State switched from none to eastWest
                self.eastState = .green
                self.westState = .green
            }
        case .southNorth:
            if ((self.eastState == .green) && (self.westState == .green)) {
                // State switched from eastWest to southNorth
                self.eastState = .amber
                self.westState = .amber
                
                DispatchQueue.main.asyncAfter(deadline: waitDuration, execute: {
                    if let _ = self.animateLightsTimer {
                        self.eastState = .red
                        self.southState = .green
                        self.westState = .red
                        self.northState = .green
                    }
                })
            } else {
                // State swithed from none to southNorth
                self.southState = .green
                self.northState = .green
            }
        }
    }
}
