//
//  CardinalDirectionState.swift
//  Traffic Lights
//
//  Created by Sheldon So on 7/05/17.
//
//

import Foundation

enum CardinalDirectionState: Int {
    case none = 0           // All traffic lights are red, no traffic is allowed
    
    case eastWest = 1       // East and West lights are green, traffic is allowed from east to west and vice versa
    case southNorth = 2     // South and North lights are green, traffic is allowed from south to north
}
