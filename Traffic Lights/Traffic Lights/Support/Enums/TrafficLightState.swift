//
//  TrafficLightState.swift
//  Traffic Lights
//
//  Created by Sheldon So on 7/05/17.
//
//

import Foundation

enum TrafficLightState: Int {
    case notSet = 0     // Traffic light has never been set
    
    case red = 1        // Traffic light is red
    case amber = 2      // Traffic light is yellow
    case green = 3      // Traffic light is green
}
