//
//  StartStopState.swift
//  Traffic Lights
//
//  Created by Sheldon So on 7/05/17.
//
//

import Foundation

enum StartStopState: Int {
    case notSet = 0     // Button never tapped
    
    case starting = 1   // Button has been tapped from stop to start, switching state
    case started = 2    // All traffic lights animation started
    case stopping = 3   // Button has been tapped from start to stop, switching state
    case stopped = 4    // All traffic lights animation stopped
}
