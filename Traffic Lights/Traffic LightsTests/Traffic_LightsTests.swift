//
//  Traffic_LightsTests.swift
//  Traffic LightsTests
//

import XCTest
@testable import Traffic_Lights

class Traffic_LightsTests: XCTestCase {
    
    var vc: Traffic_Lights.TrafficLightsViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        vc = storyboard.instantiateViewController(withIdentifier: "ID_TrafficLightsViewController") as! Traffic_Lights.TrafficLightsViewController
        let _ = vc.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFullCycle() {
        // Initial state before button tapped
        XCTAssertEqual(vc.northTrafficLightImageView.image, UIImage(named: "red"))
        XCTAssertEqual(vc.westTrafficLightImageView.image, UIImage(named: "red"))
        XCTAssertEqual(vc.southTrafficLightImageView.image, UIImage(named: "red"))
        XCTAssertEqual(vc.eastTrafficLightImageView.image, UIImage(named: "red"))
        XCTAssertEqual(vc.northState, .notSet)
        XCTAssertEqual(vc.westState, .notSet)
        XCTAssertEqual(vc.southState, .notSet)
        XCTAssertEqual(vc.eastState, .notSet)
        XCTAssertEqual(vc.buttonState, .notSet)
        XCTAssertEqual(vc.allowingCardinalDirectionsState, .none)
        
        // Button tapped
        vc.startStopButtonTapped(vc.startStopButton)
        
        // Half a second is chosen as the latency to allow the app to update the UI
        // Wait for 0.5 seconds and re-evaluate state
        let firstDelay = 0.5
        let firstExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + firstDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.northState, .notSet)
            XCTAssertEqual(self.vc.westState, .notSet)
            XCTAssertEqual(self.vc.southState, .notSet)
            XCTAssertEqual(self.vc.eastState, .notSet)
            XCTAssertEqual(self.vc.buttonState, .starting)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .none)
            firstExpectation.fulfill()
        }
        
        // Wait for 5.5 seconds and re-evaluate state
        let secondDelay = vc.yellowLightDuration + 0.5
        let secondExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + secondDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "green"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "green"))
            XCTAssertEqual(self.vc.northState, .notSet)
            XCTAssertEqual(self.vc.westState, .green)
            XCTAssertEqual(self.vc.southState, .notSet)
            XCTAssertEqual(self.vc.eastState, .green)
            XCTAssertEqual(self.vc.buttonState, .started)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .eastWest)
            secondExpectation.fulfill()
        }
        
        // Wait for 35.5 seconds and re-evaluate state
        let thirdDelay = vc.yellowLightDuration + vc.trafficLightSwitchDuration + 0.5
        let thirdExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + thirdDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.northState, .notSet)
            XCTAssertEqual(self.vc.westState, .amber)
            XCTAssertEqual(self.vc.southState, .notSet)
            XCTAssertEqual(self.vc.eastState, .amber)
            XCTAssertEqual(self.vc.buttonState, .started)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .southNorth)
            thirdExpectation.fulfill()
        }
        
        // Wait for 40.5 seconds and re-evaluate state
        let fourthDelay = 2*vc.yellowLightDuration + vc.trafficLightSwitchDuration + 0.5
        let fourthExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + fourthDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "green"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "green"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.northState, .green)
            XCTAssertEqual(self.vc.westState, .red)
            XCTAssertEqual(self.vc.southState, .green)
            XCTAssertEqual(self.vc.eastState, .red)
            XCTAssertEqual(self.vc.buttonState, .started)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .southNorth)
            fourthExpectation.fulfill()
        }
        
        // Wait for 70.5 seconds and re-evaluate state
        let fifthDelay = 2*vc.yellowLightDuration + 2*vc.trafficLightSwitchDuration + 0.5
        let fifthExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + fifthDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.northState, .amber)
            XCTAssertEqual(self.vc.westState, .red)
            XCTAssertEqual(self.vc.southState, .amber)
            XCTAssertEqual(self.vc.eastState, .red)
            XCTAssertEqual(self.vc.buttonState, .started)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .eastWest)
            fifthExpectation.fulfill()
        }
        
        // Wait for 75.5 seconds and re-evaluate state
        let sixthDelay = 3*vc.yellowLightDuration + 2*vc.trafficLightSwitchDuration + 0.5
        let sixthExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + sixthDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "green"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "green"))
            XCTAssertEqual(self.vc.northState, .red)
            XCTAssertEqual(self.vc.westState, .green)
            XCTAssertEqual(self.vc.southState, .red)
            XCTAssertEqual(self.vc.eastState, .green)
            XCTAssertEqual(self.vc.buttonState, .started)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .eastWest)
            sixthExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 3*vc.yellowLightDuration + 2*vc.trafficLightSwitchDuration + 1.0, handler: nil)
    }

    func testStopButtonTappedWhenCardinalDirectionEastWestGreen() {
        // Button tapped
        vc.startStopButtonTapped(vc.startStopButton)
        
        // Half a second is chosen as the latency to allow the app to update the UI
        // Press stop button after waiting for 5.5 seconds
        let buttonDelay = vc.yellowLightDuration + 0.5
        DispatchQueue.main.asyncAfter(deadline: .now() + buttonDelay) {
            self.vc.startStopButtonTapped(self.vc.startStopButton)
        }
        
        // Wait for 0.5 seconds after pressing the stop button
        let firstDelay = buttonDelay + 0.5
        let firstDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + firstDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.northState, .red)
            XCTAssertEqual(self.vc.westState, .amber)
            XCTAssertEqual(self.vc.southState, .red)
            XCTAssertEqual(self.vc.eastState, .amber)
            XCTAssertEqual(self.vc.buttonState, .stopping)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .none)
            firstDelayExpectation.fulfill()
        }
        
        // Wait for 5.5 seconds after pressing the stop button
        let secondDelay = firstDelay + vc.yellowLightDuration + 0.5
        let secondDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + secondDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.northState, .red)
            XCTAssertEqual(self.vc.westState, .red)
            XCTAssertEqual(self.vc.southState, .red)
            XCTAssertEqual(self.vc.eastState, .red)
            XCTAssertEqual(self.vc.buttonState, .stopped)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .none)
            secondDelayExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: secondDelay + 0.5, handler: nil)
    }

    func testStopButtonTappedWhenCardinalDirectionEastWestYellow() {
        // Button tapped
        vc.startStopButtonTapped(vc.startStopButton)
        
        // Half a second is chosen as the latency to allow the app to update the UI
        // Press stop button after waiting for 35.5 seconds
        let buttonDelay = vc.yellowLightDuration + vc.trafficLightSwitchDuration + 0.5
        DispatchQueue.main.asyncAfter(deadline: .now() + buttonDelay) {
            self.vc.startStopButtonTapped(self.vc.startStopButton)
        }
        
        // Wait for 0.5 seconds after pressing the stop button
        let firstDelay = buttonDelay + 0.5
        let firstDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + firstDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.northState, .red)
            XCTAssertEqual(self.vc.westState, .amber)
            XCTAssertEqual(self.vc.southState, .red)
            XCTAssertEqual(self.vc.eastState, .amber)
            XCTAssertEqual(self.vc.buttonState, .stopping)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .none)
            firstDelayExpectation.fulfill()
        }
        
        // Wait for 5.5 seconds after pressing the stop button
        let secondDelay = firstDelay + vc.yellowLightDuration + 0.5
        let secondDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + secondDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.northState, .red)
            XCTAssertEqual(self.vc.westState, .red)
            XCTAssertEqual(self.vc.southState, .red)
            XCTAssertEqual(self.vc.eastState, .red)
            XCTAssertEqual(self.vc.buttonState, .stopped)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .none)
            secondDelayExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: secondDelay + 0.5, handler: nil)
    }
    
    func testStopButtonTappedWhenCardinalDirectionSouthNorthGreen() {
        // Button tapped
        vc.startStopButtonTapped(vc.startStopButton)

        // Half a second is chosen as the latency to allow the app to update the UI
        // Press stop button after waiting for 40.5 seconds
        let buttonDelay = 2*vc.yellowLightDuration + vc.trafficLightSwitchDuration + 0.5
        DispatchQueue.main.asyncAfter(deadline: .now() + buttonDelay) {
            self.vc.startStopButtonTapped(self.vc.startStopButton)
        }

        // Wait for 0.5 seconds after pressing the stop button
        let firstDelay = buttonDelay + 0.5
        let firstDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + firstDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.northState, .amber)
            XCTAssertEqual(self.vc.westState, .red)
            XCTAssertEqual(self.vc.southState, .amber)
            XCTAssertEqual(self.vc.eastState, .red)
            XCTAssertEqual(self.vc.buttonState, .stopping)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .none)
            firstDelayExpectation.fulfill()
        }

        // Wait for 5.5 seconds after pressing the stop button
        let secondDelay = firstDelay + vc.yellowLightDuration + 0.5
        let secondDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + secondDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.northState, .red)
            XCTAssertEqual(self.vc.westState, .red)
            XCTAssertEqual(self.vc.southState, .red)
            XCTAssertEqual(self.vc.eastState, .red)
            XCTAssertEqual(self.vc.buttonState, .stopped)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .none)
            secondDelayExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: secondDelay + 0.5, handler: nil)
    }
    
    func testStopButtonTappedWhenCardinalDirectionSouthNorthYellow() {
        // Button tapped
        vc.startStopButtonTapped(vc.startStopButton)
        
        // Half a second is chosen as the latency to allow the app to update the UI
        // Press stop button after waiting for 70.5 seconds
        let buttonDelay = 2*vc.yellowLightDuration + 2*vc.trafficLightSwitchDuration + 0.5
        DispatchQueue.main.asyncAfter(deadline: .now() + buttonDelay) {
            self.vc.startStopButtonTapped(self.vc.startStopButton)
        }
        
        // Wait for 0.5 seconds after pressing the stop button
        let firstDelay = buttonDelay + 0.5
        let firstDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + firstDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "amber"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.northState, .amber)
            XCTAssertEqual(self.vc.westState, .red)
            XCTAssertEqual(self.vc.southState, .amber)
            XCTAssertEqual(self.vc.eastState, .red)
            XCTAssertEqual(self.vc.buttonState, .stopping)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .none)
            firstDelayExpectation.fulfill()
        }
        
        // Wait for 5.5 seconds after pressing the stop button
        let secondDelay = firstDelay + vc.yellowLightDuration + 0.5
        let secondDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + secondDelay) {
            XCTAssertEqual(self.vc.northTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.westTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.southTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.eastTrafficLightImageView.image, UIImage(named: "red"))
            XCTAssertEqual(self.vc.northState, .red)
            XCTAssertEqual(self.vc.westState, .red)
            XCTAssertEqual(self.vc.southState, .red)
            XCTAssertEqual(self.vc.eastState, .red)
            XCTAssertEqual(self.vc.buttonState, .stopped)
            XCTAssertEqual(self.vc.allowingCardinalDirectionsState, .none)
            secondDelayExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: secondDelay + 0.5, handler: nil)
    }
    
    func testStartStopButtonTappedWhenStartStopStateNotSet() {
        // Button tapped
        vc.startStopButtonTapped(vc.startStopButton)
        
        // Wait for 1.0 seconds after pressing the stop button
        let delay = 1.0
        let delayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            XCTAssertEqual(self.vc.buttonState, .starting)
            delayExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: delay + 1.0, handler: nil)
    }

    func testStartStopButtonTappedWhenStartStopStateStarting() {
        // Button tapped
        vc.startStopButtonTapped(vc.startStopButton)
        
        // Half a second is chosen as the latency to allow the app to update the UI
        // Press stop button after waiting for 1.0 seconds
        let buttonDelay = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + buttonDelay) {
            self.vc.startStopButtonTapped(self.vc.startStopButton)
        }
        
        // Wait for 1.0 seconds after pressing the stop button
        let firstDelay = buttonDelay + 1.0
        let firstDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + firstDelay) {
            XCTAssertEqual(self.vc.buttonState, .starting)
            firstDelayExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: firstDelay + 1.0, handler: nil)
    }
    
    func testStartStopButtonTappedWhenStartStopStateStarted() {
        // Button tapped
        vc.startStopButtonTapped(vc.startStopButton)
        
        // Half a second is chosen as the latency to allow the app to update the UI
        // Press stop button after waiting for 1.0 seconds
        let buttonDelay = vc.yellowLightDuration + 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + buttonDelay) {
            self.vc.startStopButtonTapped(self.vc.startStopButton)
        }
        
        // Wait for 1.0 seconds after pressing the stop button
        let firstDelay = buttonDelay + 1.0
        let firstDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + firstDelay) {
            XCTAssertEqual(self.vc.buttonState, .stopping)
            firstDelayExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: firstDelay + 1.0, handler: nil)
    }
    
    func testStartStopButtonTappedWhenStartStopStateStopping() {
        // Button tapped
        vc.startStopButtonTapped(vc.startStopButton)
        
        // Half a second is chosen as the latency to allow the app to update the UI
        // Press stop button after waiting for 1.0 seconds
        let firstButtonDelay = vc.yellowLightDuration + 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + firstButtonDelay) {
            self.vc.startStopButtonTapped(self.vc.startStopButton)
        }
        
        // Press stop button after waiting for another 1.0 second
        let secondButtonDelay = firstButtonDelay + 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + secondButtonDelay) {
            self.vc.startStopButtonTapped(self.vc.startStopButton)
        }
        
        // Wait for 1.0 seconds after pressing the stop button the second time
        let firstDelay = secondButtonDelay + 1.0
        let firstDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + firstDelay) {
            XCTAssertEqual(self.vc.buttonState, .stopping)
            firstDelayExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: firstDelay + 1.0, handler: nil)
    }
    
    func testStartStopButtonTappedWhenStartStopStateStopped() {
        // Button tapped
        vc.startStopButtonTapped(vc.startStopButton)
        
        // Half a second is chosen as the latency to allow the app to update the UI
        // Press stop button after waiting for 1.0 seconds
        let firstButtonDelay = vc.yellowLightDuration + 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + firstButtonDelay) {
            self.vc.startStopButtonTapped(self.vc.startStopButton)
        }
        
        // Press stop button after waiting for another 1.0 second
        let secondButtonDelay = firstButtonDelay + vc.yellowLightDuration + 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + secondButtonDelay) {
            self.vc.startStopButtonTapped(self.vc.startStopButton)
        }
        
        // Wait for 1.0 seconds after pressing the start button the second time
        let firstDelay = secondButtonDelay + 1.0
        let firstDelayExpectation = self.expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + firstDelay) {
            XCTAssertEqual(self.vc.buttonState, .starting)
            firstDelayExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: firstDelay + 1.0, handler: nil)
    }
}
