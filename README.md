# Safety Culture Traffic Lights Test's README
----------

## Introduction ##
* The logic of this application is implemented in a way that is similar to how traffic lights operate in the real world.
* This application is implemented to stimulate several finite state machines. When a state is changed, UI updates accordingly.

## Traffic Lights ##
* Each traffic light contains four possible states:
* * Not Set - Initially all four traffic lights are not set, which will be displayed as red.
* * Red - The traffic light has either been switched from green (to yellow) to red.
* * Amber - The traffic light is in the middle of switching from green to red.
* * Green - The traffic light has been switched from not set or red to green.

## Cardinal Direction ##
* Treat this application as an intersection. Traffic will either flow in the direction of east and west, or in the direction of south and north. i.e. the traffic lights on the right of the screen suggests that traffic is allowed to travel from right (east) to left (south).
* It is assumed that turning is not allowed within this application.
* No traffic will be allowed in all directions after 5 seconds pause (same duration as the yellow lights' duration) when the stop button is tapped.

## Button ##
* During the event of switching lights (from green to red), yellow lights will be presented and the start/stop button will be disabled.
* While the start/stop button is disabled, a title of the button is updated periodically to describe when it will be available.
* When switching from not set or stop to start, the application requires 5 seconds pause (same duration as the yellow lights' duration) in order for the application to function properly.

## Unit Tests ##
* The following are the unit tests that were performed. It might not have fully covered all scenarios, but it definitely does its jobs in fixing some potential defects. ;)
* For each of the following unit tests, a latency of 0.5/1.0 seconds are added to ensure it provides enough time for the application to update its UI.
* * testFullCycle - Inspect states and UI for 75 seconds (5+30+5+30+5) after tapping the start button.
* * testStopButtonTappedWhenCardinalDirectionEastWestGreen - Inspect states and UI changes after tapping the stop button when the traffic lights east and west are green.
* * testStopButtonTappedWhenCardinalDirectionEastWestYellow - Inspect states and UI changes after tapping the stop button when the traffic lights east and west are yellow (switching from green to red).
* * testStopButtonTappedWhenCardinalDirectionSouthNorthGreen - Inspect states and UI changes after tapping the stop button when the traffic lights south and north are green.
* * testStopButtonTappedWhenCardinalDirectionSouthNorthYellow - Inspect states and UI changes after tapping the stop button when the traffic lights south and north are yellow (switching from green to red).
* * testStartStopButtonTappedWhenStartStopStateNotSet - Inspect states and UI changes when the user attempts to tap on the button initially.
* * testStartStopButtonTappedWhenStartStopStateStarting - Inspect states and UI changes when the user attempts to tap on the button when the animation is starting.
* * testStartStopButtonTappedWhenStartStopStateStarted - Inspect states and UI changes when the user attempts to tap on the button when the traffic lights are operating.
* * testStartStopButtonTappedWhenStartStopStateStopping - Inspect states and UI changes when the user attempts to tap on the button when the traffic light is stopping.
* * testStartStopButtonTappedWhenStartStopStateStopped - Inspect states and UI changes when the user attempts to tap on the button when the traffic lights have stopped operating.

----------
Feel free to send me an email for any question.

Regards,

Sheldon